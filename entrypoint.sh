#!/bin/bash


# Evaluar si el comando ping existe, de lo contrario se instala.

if which ping;then

	echo "no se hace nada"

else
	apt update && apt install iputils-ping -y

fi



while :;do
# La variable de entorno 'DHOST' contiene la ip o nombre de hosts a donde se deba dirigir el ping.
	ping -c 1 $DHOST | tee -a /ping.output
	sleep 10

done
# Ejecutar el comando declado en el Dockerfile bajo la intruccion 'CMD'.


exec "$@"


