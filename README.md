# Docker Ping

Proyecto para implementar `Dockerfile`.

### Creacion de imagen y despliegue

Imagen.

```
$ git clone git@gitlab.com:sergio.pernas1/docker-ping.git
$ 
$ docker build -t docker-ping:debian .
```

Contenedor.

```
$ docker run docker-ping:debian
```

### Consideraciones


Cuano lo ejecutamos por primera vez el proceso ping devuelve su salida por la terminal donde se lanzo el contenedor, al detener el proceso ping se detiene el contenedor, porque en defintiva un contenedor es un proceso en ejecucion.

```
$ docker ps -a --format "table {{.Status}}\t{{.Command}}\t{{.Names}}" | grep -vi "up"
STATUS                        COMMAND                  NAMES
Exited (137) 13 minutes ago   "ping google.com"        reverent_lalande
```


El contenedor se crea y se ejecuta su comando con 'run' pero para un contenedor existente usamos el comando 'start' para iniciar y 'stop' para detenerlo.

```
$ docker start reverent_lalande
```

Una vez ejecutado la salida del proceso contenido ya no se muestra en terminal, por que de hecho ya no esta asociado a ninguna terminal ni shell.

```
$ docker ps -a --format "table {{.Status}}\t{{.Command}}\t{{.Names}}" | grep reve
Up 13 seconds   "ping google.com"        reverent_lalande
```


Todo registro de eventos (logs) y salidas de procesos en el contenedor son recogidas por docker y podemos verlos con el comando 'logs'.

```
$ docker logs reverent_lalande
...
64 bytes from eze06s02-in-f14.1e100.net (172.217.172.110): icmp_seq=209 ttl=58 time=21.3 ms
64 bytes from eze06s02-in-f14.1e100.net (172.217.172.110): icmp_seq=210 ttl=58 time=17.8 ms
```
